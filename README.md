# Results
|method name|method explanation                          |execution time[^1]|
|-----------|--------------------------------------------|------------------|
|ls-native  |Native directory listing using os.ReadDir   |~800ms            |
|ls-cmd     |Counting lines in `ls` command output       |~5s               |
|pm-cmd     |Counting lines in `pacman -Q` command output|~23s              |

[^1]: At the moment of the testing I have 741 packages installed and each method is ran 1000 times to smooth out the results.
