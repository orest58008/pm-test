package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

func main() {
    start := time.Now()
    pmDir := "/var/lib/pacman/local/"
    count := 0
    if os.Args[1] == "ls-cmd" {
	for i := 0; i < 1000; i++ {
	    out, err := exec.Command("ls", "-1", pmDir).Output()
	    if err != nil { log.Fatal(err) }
	    count = strings.Count(string(out), "\n") - 1
	}
    } else if os.Args[1] == "ls-native" {
	for i := 0; i < 1000; i++ {
	    entries, err := os.ReadDir(pmDir)
	    if err != nil { log.Fatal(err) }
	    count = len(entries) - 1
	}
    } else if os.Args[1] == "pm-cmd" {
	for i := 0; i < 1000; i++ {
	    out, err := exec.Command("pacman", "-Q").Output()
	    if err != nil { log.Fatal(err) }
	    count = strings.Count(string(out), "\n")
	}
    }

    fmt.Print(count)
    fmt.Println(" packages")
    fmt.Println(time.Since(start))
}
